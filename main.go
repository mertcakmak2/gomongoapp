package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"gotodoapp/model"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//EndPoint's
// => GET "/"           	-test
// => GET "/todo"			-tüm todoları getirir.
// => POST "/todo"			-todo ekler.
// => GET "/todo/update/3" 	-3 id'li kaydın statu alanını günceller.
// => DELETE "/todo/8"		-8 id'li todoyu siler.

func main() {

	fmt.Println("test")

	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://mertcakmak:8NIpweksnlXJJZo9@cluster0.925ew.mongodb.net/todoapp?retryWrites=true&w=majority"))

	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)

	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	e.GET("/test", func(c echo.Context) error {
		return c.String(http.StatusOK, "test mongov6")
	})

	//Tüm todoları getirir.
	e.GET("/todo", func(c echo.Context) error {
		var results []*model.Todo
		db := client.Database("todoapp")
		collection := db.Collection("todo")
		res, err := collection.Find(context.TODO(), bson.D{}, options.Find())
		if err != nil {
			fmt.Println("error")
			return c.JSON(http.StatusNotFound, err)
		}

		for res.Next(context.TODO()) {
			var elem model.Todo
			err := res.Decode(&elem)
			if err != nil {
				log.Fatal(err)
				return nil
			}
			results = append(results, &elem)
		}

		return c.JSON(http.StatusOK, results)
	})

	//Alınan todoyu kaydeder.
	e.POST("/todo", func(c echo.Context) error {
		todo := new(model.Todo)
		if err = c.Bind(todo); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}

		db := client.Database("todoapp")
		collection := db.Collection("todo")
		res, err := collection.InsertOne(context.Background(), todo)

		if err != nil {
			fmt.Println("error")
			return c.JSON(http.StatusNotFound, err)
		}

		return c.JSON(http.StatusOK, res)
	})

	//Todonun statu'sünü günceller.
	e.GET("/todo/update/:id", func(c echo.Context) error {
		db := client.Database("todoapp")
		collection := db.Collection("todo")
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		opts := options.Update().SetUpsert(false)
		filter := bson.D{{"_id", id}}
		update := bson.D{{"$set", bson.D{{"statu", "1"}}}}
		res, err := collection.UpdateOne(context.TODO(), filter, update, opts)

		if err != nil {
			fmt.Println("error")
			return c.JSON(http.StatusNotFound, err)
		}

		return c.JSON(http.StatusOK, res)
	})

	//Todoyu siler
	e.DELETE("/todo/:id", func(c echo.Context) error {
		db := client.Database("todoapp")
		collection := db.Collection("todo")
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		res, err := collection.DeleteOne(context.Background(), bson.M{"_id": id})
		if err != nil {
			fmt.Println("error")
			return c.JSON(http.StatusNotFound, err)
		}

		return c.JSON(http.StatusOK, res)
	})

	e.Logger.Fatal(e.Start(":8080"))

}
