package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type (
	Todo struct {
		ID          primitive.ObjectID `bson:"_id,omitempty"`
		Description string             `json:"Description"`
		Statu       string             `json:"Statu"`
	}
)
