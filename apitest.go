package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gotodoapp/model"
	"log"
	"net/http"
	"testing"
)

// => go clean -testcache

func TestSaveTodo(t *testing.T) {
	newTodo := model.Todo{Description: "go unit testt", Statu: "0"}
	json_data, err := json.Marshal(newTodo)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := http.Post("http://164.90.163.134:8080/todo", "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		log.Fatal(err)
	}

	if resp.StatusCode != 200 {
		t.Errorf("Failed")
	}
}

func TestDeleteTodoByID(t *testing.T) {

	client := &http.Client{}

	req, err := http.NewRequest("DELETE", "http://164.90.163.134:8080/todo/607e7add93fe34ceb9fec68c", nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		t.Errorf("Failed")
	}
}

func TestUpdateTodoByID(t *testing.T) {

	resp, err := http.Get("http://164.90.163.134:8080/todo/update/607e7a7693fe34ceb9fec68a")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		t.Errorf("Failed")
	}
}
